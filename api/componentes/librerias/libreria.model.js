'use strict';
let mongoose = require ('mongoose'); 
let libreria_schema = new mongoose.Schema({

nombre : {type : String, unique : true, require : true},
nombre_juridico : {type : String,require :true},
codigo_sucursal : {type : String, require : true},
email : {type : String, require : true},
telefono : {type : String, require : true},
provincia : {type : String,require :true},
canton : {type : String,require :true},
extra : {type : String, required : true},
estado : {type : String, required : true},


});
module.exports = mongoose.model('librerias',libreria_schema);