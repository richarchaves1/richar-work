'use strict';
const autorModel = require ('./autor.model');

module.exports.registrar_autor = function(req, res) {

  let nuevoAutor = new autorModel({
    nombre : req.body.nombre,
    biografia : req.body.biografia,   
    genero : req.body.genero,
    libros : req.body.libros,
    estado : 'Activo'
   });   

   nuevoAutor.save(function(error){
      if(error){

        res.json({
          success : false,
          msj: 'El autor no pudo ser registrado : '+ error

        });
     }else{
           res.json({
              success : true,
              msj: 'El autor se registro correctamente '

           });

      }


   });

};

module.exports.listar_autores = function(req,res){

autorModel.find().then(
    function(autores){
       if(autores.length > 0){
        res.json({
            success : true,
            lista_autores : autores
        });

       }else{
             res.json({
                success : false,
                lista_autores : autores
             });
       }
    }
);

};

module.exports.buscar_por_id = function (req, res){
    autorModel.find({_id : req.body.id_autor}).then(
       function(autor){
           if(autor){
               res.json({success: true, autor : autor});
           }else{
               res.json({success: false, autor : autor});
           }
       }

   );

};

module.exports.actualizar = function(req, res){
  
   autorModel.findByIdAndUpdate(req.body.id_autor, { $set: req.body },
       function (error){
           if(error){
               res.json({success : false , msg : 'No se pudo actualizar el autor'});
           }else{
               res.json({success: true , msg : 'El autor se actualizó con éxito'});
           }
       }
   
   );
}

module.exports.delete = function(req, res){
   
    autorModel.findByIdAndRemove(req.body.id_autor,
        function (error){
            if(error){
                res.json({success : false , msg : 'No se pudo eliminar el autor'});
            }else{
                res.json({success: true , msg : 'El autor se eliminó con éxito'});
            }
        }
    
    );

    
 };
