'use strict';
let mongoose = require ('mongoose'); 
let autorSchema = new mongoose.Schema({

    nombre : {type : String, require : true},
    biografia : {type : String, require : true},
    genero : {type : String,require :true},
    libros : {type :String, require : true},
    estado : {type: String, required : true},

});
module.exports = mongoose.model('autor',autorSchema);