'use strict';
let mongoose = require ('mongoose'); 
let libroSchema = new mongoose.Schema({

titulo : {type : String, unique : true, require : true},
precio : {type : Number,require :true},
anioEd : {type : Number, require : true},
edicion : {type : String, require : true},
genero : {type : String,require :true},
autor : {type : String,require :true},
libroIsbn : {type :String, require : true },
descrip : {type :String, require : true },
tipoLibro :{type :String, require : true },
cantidad : {type :Number, require : true },
imgPortada :{type :String, require : true },
imgContraPortada :{type :String, require : true },
estado : {type : String, required : true},
califica :{type : Number, required : true},
rating1 :{type : Number, required : true},
rating2 :{type : Number, required : true},
rating3 :{type : Number, required : true},
rating4 :{type : Number, required : true},
rating5 :{type : Number, required : true}



});
module.exports = mongoose.model('libros',libroSchema);