'use strict';
const express = require ('express');
const router = express.Router();
const librosApi = require ('./libro.api');

router.param('id_libro', function(req, res, next, id_libro){
  req.body.id_libro = id_libro;
  next();
});
router.param('autor', function(req, res, next, autor){
  req.body.autor = autor;
  next();
});
router.param('titulo', function(req, res, next, titulo){
  req.body.titulo = titulo;
  next();
});

router.param('isbn', function(req, res, next, isbn){
  req.body.isbn = isbn;
  next();
});

router.param('gen', function(req, res, next, gen){
  req.body.gen = gen;
  next();
});

router.route('/registrar_libro')
   .post(function(req,res){

         librosApi.registrar_libro(req, res);

    });
    

    router.route('/actualizar_libro')
    .post(
        function(req , res){
          librosApi.actualizar(req, res);
        }
    );


    router.route('/listar_libro')
    .get(
      function(req,res){
 
      librosApi.listar(req, res);
 
     });    
 

router.route('/buscar_libro/:id_libro')
.get(
  function(req , res){
      librosApi.buscar_por_id(req, res);
  }
);
router.route('/eliminar_libro/:id_libro')
    .post(
        function(req , res){
          librosApi.delete(req, res);
        }
    );

router.route('/buscar_libroAutor/:autor')
    .get(
        function(req , res){
          librosApi.FindByAutores(req, res);
        }
);
router.route('/buscar_libroTitle/:titulo')
    .get(
        function(req , res){
          librosApi.FindByTitle(req, res);
        }
);

router.route('/buscar_libroISBN/:isbn')
    .get(
        function(req , res){
          librosApi.FindByIsbn(req, res);
        }
);
router.route('/buscar_libroGen/:gen')
    .get(
        function(req , res){
          librosApi.FindByGen(req, res);
        }
);


module.exports = router;