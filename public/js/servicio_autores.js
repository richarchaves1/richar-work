'use strict';

function registrar_autor(pnombre, pbiografia ,pgenero , plibros){
    let request = $.ajax({
        url : 'http://localhost:27017/api/registrar_autor',
        method : "POST",
        data : {
            nombre : pnombre,
            biografia : pbiografia,
            genero : pgenero,
            libros: plibros,
            estado : 'Activo'
        },
        dataType : "json",
        contentType : 'application/x-www-form-urlencoded; charset=UTF-8' 
    });
    
    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });
};

let listar_autores = () => {

    let autores = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/listar_autores",
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        autores = res.lista_autores;
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return autores;
  };

let buscar_autor = (id_autor) => {

    let autor = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/buscar_autor/"+ id_autor ,
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        autor = res.autor;
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return autor;
  };


let actualizar_autor = (pnombre, pbiografia, pgenero, plibros, pid_autor) =>{
    let request = $.ajax({
        url : 'http://localhost:27017/api/actualizar_autor',
        method : "POST",
        data : {
            nombre : pnombre,
            biografia : pbiografia,
            genero : pgenero,
            libros: plibros,
            id_autor : pid_autor
        },
        dataType : "json",
        contentType : 'application/x-www-form-urlencoded; charset=UTF-8' 
    });

    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });

};

let borrar_autor = (id_autor) => {
  
  
    let request = $.ajax({
      url: "http://localhost:27017/api/borrar_autor/"+ id_autor,
      method: "POST",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });
   
  };
