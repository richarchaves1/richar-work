'use strict';

const input_titulo = document.querySelector('#txt_titulo');
const input_precio = document.querySelector('#txt_precio');
const input_libroIsbn = document.querySelector('#txt_Isbn');
const input_cantidad = document.querySelector('#txt_cantidad');
const input_imgPortada = document.getElementById('img_preview');
const input_imgContraPortada = document.getElementById('img_preview2');
const btn_actualizar = document.querySelector('#btn_actualizar');
const input_frmAc = document.getElementById("frm_contactoAc");


let get_param = (param) => {
    var url_string =  window.location.href;
    var url = new URL(url_string);
    var id = url.searchParams.get(param);//Toma el parámetro id_inmueble del url y retorna el valor
    return id;
};


let _id = get_param('id_libros');

let libros = buscar_libro(_id);

let mostrar_datos =     () =>{
    input_titulo.value = libros[0]['titulo'];
    input_precio.value = libros[0]['precio'];
    input_libroIsbn.value = libros[0]['libroIsbn'];

    input_cantidad.value = libros[0]['cantidad'];

    input_imgPortada.src = libros[0]['imgPortada'];
    input_imgContraPortada.src = libros[0]['imgContraPortada'];

    input_precio.min=libros[0]['precio'];
};

if(libros){
    mostrar_datos();
}

let obtener_datos = () =>{
    let titulo = libros[0]['titulo'];
    let precio = input_precio.value;
    let anioEd = libros[0]['anioEd'];
    let edicion = libros[0]['edicion'];
    let genero = libros[0]['genero'];
    let autor = libros[0]['autor'];
    let libroIsbn = libros[0]['libroIsbn'];
    let descrip = libros[0]['descrip'];
    let tipoLibro = libros[0]['tipoLibro'];
    let cantidad  = libros[0]['cantidad'];
    let imgPortada = libros[0]['imgPortada'];
    let imgContraPortada = libros[0]['imgContraPortada'];
    let califica = libros[0]['califica'];
    let rating1 = libros[0]['rating1'];
    let rating2 = libros[0]['rating2'];
    let rating3 = libros[0]['rating3'];
    let rating4 = libros[0]['rating4'];
   let rating5 = libros[0]['rating5'];
    
    if (!input_frmAc.checkValidity()) {
          
    } else {
        actualizar_libro(titulo, precio ,anioEd, edicion,genero,autor ,libroIsbn,descrip,
            tipoLibro,cantidad,imgPortada,imgContraPortada,califica,rating1,rating2,rating3,rating4,rating5, _id); 
        window.location.href = 'listEditarPrecioLibros.html';
    } 
    
    
};

btn_actualizar.addEventListener('click', obtener_datos);