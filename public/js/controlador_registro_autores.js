'use strict';

const boton_registrar = document.querySelector('#btn_registrar');

const input_nombre = document.querySelector('#txt_nombre');
const input_biografia = document.querySelector('#txt_biografia');
const input_genero = document.querySelector('#txt_genero');
const input_libros = document.querySelector('#txt_libros');

boton_registrar.addEventListener('click', obtener_datos);

function obtener_datos(){

    let nombre = input_nombre.value;
    let biografia = input_biografia.value;
    let genero = input_genero.value;
    let libros = input_libros.value;

    registrar_autor(nombre, biografia, genero, libros);
    window.location.href = 'lista_autores.html';
};

