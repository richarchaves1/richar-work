'use strict';

const boton_registrar = document.querySelector('#btn_registrar');
const input_nombre = document.querySelector('#txt_nombre');
const input_nombre_juridico = document.querySelector('#txt_nombre_juridico');
const input_codigo_sucursal = document.querySelector('#txt_codigo_sucursal');
const input_email  = document.querySelector('#txt_email');
const input_telefono = document.querySelector("#txt_telefono");
const input_provincia = document.querySelector("#slt_provincias");
const input_canton = document.querySelector('#slt_cantones');
const input_extra = document.querySelector('#txt_extra');


let get_param = (param) => {
    var url_string =  window.location.href;
    var url = new URL(url_string);
    var id = url.searchParams.get(param);//Toma el parámetro id_inmueble del url y retorna el valor
    return id;
};


let _id = get_param('id_librerias');

let librerias = buscar_libreria(_id);

let mostrar_datos =     () =>{
    input_nombre.value = librerias[0]['nombre'];
    input_nombre_juridico.value = librerias[0]['nombre_juridico'];
    input_codigo_sucursal.value = librerias[0]['codigo_sucursal'];
    input_email.value = librerias[0]['email'];
    input_telefono.value = librerias[0]['telefono'];
    input_extra.value = librerias[0]['extra']
    //Opccion genero Gen
    let opciones_provincia = document.querySelectorAll('#slt_provincias option');   
    for(let i = 0; i < opciones_provincia.length; i++){
        if(opciones_provincia.selectedOptions[i].textContent == librerias[0]['provincia']){
            opciones_provincia[i].selected = true;
           
        }
    }
//gen autor
let opciones_autor = document.querySelectorAll('cantones');   
for(let i = 0; i < opciones_autor.length; i++){
    if(input_provincia.selectedOptions[i].textContent == librerias[0]['autor']){
        opciones_autor[i].selected = true;
       //input_provincia.selectedOptions[0].textContent
    }
}
    input_libreriaIsbn.value = librerias[0]['libreriaIsbn'];
    input_descrip.value = librerias[0]['descrip'];
    


   
    input_cantidad.value = librerias[0]['cantidad'];
    //gen tipe libreria
let opciones_librerias = document.querySelectorAll('#slt_librerias option');
    
    for(let i = 0; i < opciones_librerias.length; i++){
        if(opciones_librerias[i].textContent == librerias[0]['tipolibreria']){
            opciones_librerias[i].selected = true;
           
        }
    }
    input_imgPortada.src = librerias[0]['imgPortada'];
    input_imgContraPortada.src = librerias[0]['imgContraPortada'];

   
};

if(librerias){
    mostrar_datos();
}

let obtener_datos = () =>{
    
    let nombre_libreria = input_nombre.value;
    let nombre_juridico = input_nombre_juridico.value;
    let codigo_sucursal = input_codigo_sucursal.value;
    let email = input_email.value;
    let telefono = input_telefono.value;
    let provincia = input_provincia.selectedOptions[0].textContent;
    let canton = input_canton.selectedOptions[0].textContent;
    let extra = input_extra.value;
    
    if (!input_frmAc.checkValidity()) {
          
    } else {
        actualizar_libreria(nombre_libreria, nombre_juridico, codigo_sucursal, email, telefono,
            provincia, canton, extra); 
        window.location.href = 'listar_librerias.html';
    } 
    
    
};

btn_actualizar.addEventListener('click', obtener_datos);