'use strict';

const input_titulo = document.querySelector('#txt_titulo');
const input_precio = document.querySelector('#txt_precio');
const input_anioEd = document.querySelector('#txt_anioEd');
const input_edicion  = document.querySelector('#txt_Edicion');
const input_genero = document.getElementById("slt_genero");
const input_autor = document.getElementById("slt_autors");
const input_libroIsbn = document.querySelector('#txt_Isbn');
const input_descrip = document.querySelector('#txt_descrip');
const input_tipoLibro =  document.getElementById("slt_libros");
const input_cantidad = document.querySelector('#txt_cantidad');
const input_imgPortada = document.getElementById('img_preview');
const input_imgContraPortada = document.getElementById('img_preview2');
const btn_actualizar = document.querySelector('#btn_actualizar');
const input_frmAc = document.getElementById("frm_contactoAc");


let get_param = (param) => {
    var url_string =  window.location.href;
    var url = new URL(url_string);
    var id = url.searchParams.get(param);//Toma el parámetro id_inmueble del url y retorna el valor
    return id;
};


let _id = get_param('id_libros');

let libros = buscar_libro(_id);

let mostrar_datos =     () =>{
    input_titulo.value = libros[0]['titulo'];
    input_precio.value = libros[0]['precio'];
    input_anioEd.value = libros[0]['anioEd'];
    input_edicion.value = libros[0]['edicion'];
    input_genero.value = libros[0]['genero'];
    //Opccion genero Gen
    let opciones_generos = document.querySelectorAll('#slt_genero option');   
    for(let i = 0; i < opciones_generos.length; i++){
        if(opciones_generos[i].textContent == libros[0]['genero']){
            opciones_generos[i].selected = true;
           
        }
    }
//gen autor
let opciones_autor = document.querySelectorAll('#slt_autors option');   
for(let i = 0; i < opciones_autor.length; i++){
    if(opciones_autor[i].textContent == libros[0]['autor']){
        opciones_autor[i].selected = true;
       
    }
}
    input_libroIsbn.value = libros[0]['libroIsbn'];
    input_descrip.value = libros[0]['descrip'];
    


   
    input_cantidad.value = libros[0]['cantidad'];
    //gen tipe libro
let opciones_libros = document.querySelectorAll('#slt_libros option');
    
    for(let i = 0; i < opciones_libros.length; i++){
        if(opciones_libros[i].textContent == libros[0]['tipoLibro']){
            opciones_libros[i].selected = true;
           
        }
    }
    input_imgPortada.src = libros[0]['imgPortada'];
    input_imgContraPortada.src = libros[0]['imgContraPortada'];

   
};

if(libros){
    mostrar_datos();
}

let obtener_datos = () =>{
    let titulo = input_titulo.value;
    let precio = input_precio.value;
    let anioEd = input_anioEd.value;
    let edicion = input_edicion.value;
    let genero = input_genero.value;
    let autor = input_autor.value;
    let libroIsbn = input_libroIsbn.value;
    let descrip = input_descrip.value;
    let tipoLibro = input_tipoLibro.value;
    let cantidad  = input_cantidad.value;
    let imgPortada = portadaSave;
    let imgContraPortada = portadaContraSave;
   let califica = libros[0]['califica'];
    let rating1 = libros[0]['rating1'];
    let rating2 = libros[0]['rating2'];
    let rating3 = libros[0]['rating3'];
    let rating4 = libros[0]['rating4'];
   let rating5 = libros[0]['rating5'];
    
    if (!input_frmAc.checkValidity()) {
          
    } else {
        actualizar_libro(titulo, precio ,anioEd, edicion,genero,autor ,libroIsbn,descrip,
            tipoLibro,cantidad,imgPortada,imgContraPortada,califica,rating1,rating2,rating3,rating4,rating5, _id); 
        window.location.href = 'listar_libros.html';
    } 
    
    
};

btn_actualizar.addEventListener('click', obtener_datos);