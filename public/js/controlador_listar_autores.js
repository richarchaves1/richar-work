'use strict';

const tabla = document.querySelector('#tbl_autores tbody');
let autores = listar_autores();

let mostrar_autores = () =>{
    for(let i = 0; i < autores.length; i++){
        let fila = tabla.insertRow();

        fila.insertCell().innerHTML = autores[i]['nombre'];
        fila.insertCell().innerHTML = autores[i]['biografia'];
        fila.insertCell().innerHTML = autores[i]['genero'];
        fila.insertCell().innerHTML = autores[i]['libros'];

        let celda_configuracion = fila.insertCell();

        // Creación del botón de editar
        let boton_editar = document.createElement('a');
        boton_editar.textContent = 'Editar';
        boton_editar.href = `actualizar_autores.html?id_autor=${autores[i]['_id']}`;

        celda_configuracion.appendChild(boton_editar);

    }

};

function search_autores() {
// Se declaran las variables 
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    table = document.getElementById("tbl_autores");
    tr = table.getElementsByTagName("tr");

// Se itera por todas las filas de la tabla y se esconden las que no coincidan con la busqueda
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }
        } 
    }
}

mostrar_autores();
