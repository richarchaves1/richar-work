'use strict';

const boton_registrar = document.querySelector('#btn_registrar');
const input_nombre = document.querySelector('#txt_nombre');
const input_nombre_juridico = document.querySelector('#txt_nombre_juridico');
const input_codigo_sucursal = document.querySelector('#txt_codigo_sucursal');
const input_email  = document.querySelector('#txt_email');
const input_telefono = document.querySelector("#txt_telefono");
const input_provincia = document.querySelector("#slt_provincias");
const input_canton = document.querySelector('#slt_cantones');
const input_extra = document.querySelector('#txt_extra');


let get_param = (param) => {
    var url_string =  window.location.href;
    var url = new URL(url_string);
    var id = url.searchParams.get(param);//Toma el parámetro id_inmueble del url y retorna el valor
    return id;
};


let _id = get_param('id_sucursales');

let sucursales = buscar_sucursales(_id);

let mostrar_datos =     () =>{
    input_nombre.value = sucursales[0]['nombre'];
    input_nombre_juridico.value = sucursales[0]['nombre_juridico'];
    input_codigo_sucursal.value = sucursales[0]['codigo_sucursal'];
    input_email.value = sucursales[0]['email'];
    input_telefono.value = sucursales[0]['telefono'];
    input_extra.value = sucursales[0]['extra']
  
    


let opciones_sucursales = document.querySelectorAll('#slt_sucursales option');
    
    for(let i = 0; i < opciones_sucursales.length; i++){
        if(opciones_sucursales[i].textContent == sucursales[0]['tiposucursales']){
            opciones_sucursales[i].selected = true;
           
        }
    }


   
};

if(sucursales){
    mostrar_datos();
}

let obtener_datos = () =>{
    
    let nombre_sucursales = input_nombre.value;
    let nombre_juridico = input_nombre_juridico.value;
    let codigo_sucursal = input_codigo_sucursal.value;
    let email = input_email.value;
    let telefono = input_telefono.value;
    let provincia = input_provincia.selectedOptions[0].textContent;
    let canton = input_canton.selectedOptions[0].textContent;
    let extra = input_extra.value;
    
    
        actualizar_sucursales(nombre_sucursales, nombre_juridico, codigo_sucursal, email, telefono,
            provincia, canton, extra); 
        window.location.href = 'listar_sucursales.html';
     
    
    
};

btn_actualizar.addEventListener('click', obtener_datos);