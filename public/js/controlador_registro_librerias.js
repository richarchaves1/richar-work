'use strict';

const boton_registrar = document.querySelector('#btn_registrar');

const input_nombre = document.querySelector('#txt_nombre');
const input_nombre_juridico = document.querySelector('#txt_nombre_juridico');
const input_codigo_sucursal = document.querySelector('#txt_codigo_sucursal');
const input_email  = document.querySelector('#txt_email');
const input_telefono = document.querySelector("#txt_telefono");
const input_provincia = document.querySelector("#slt_provincias");
const input_canton = document.querySelector('#slt_cantones');
const input_extra = document.querySelector('#txt_extra');


function obtener_datos(){
    

    let nombre_libreria = input_nombre.value;
    let nombre_juridico = input_nombre_juridico.value;
    let codigo_sucursal = input_codigo_sucursal.value;
    let email = input_email.value;
    let telefono = input_telefono.value;
    let provincia = input_provincia.selectedOptions[0].textContent;
    let canton = input_canton.selectedOptions[0].textContent;
    let extra = input_extra.value;
    let bError = validar();

  
    if(bError == false){
      registrar_mascotas(nors, nombre, raza, edad, sexo);
    }else{
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Revise los valores ingresados',
                    
      })
    }

    function validar(){
      let bError = false;
  
      if(input_nombre.value == ''){
        bError = true;
  
      }
  
      if(input_nombre_juridico.value == ''){
        bError = true;
      }
  
      if(input_email.value == ''){
        bError = true;
      }
  
      if(input_telefono.value == ''){
        bError = true;
      }
  
      if(provincia.value == ''){
        bError = true;

      }
      if(canton.value == ''){
        bError = true;
      }
      if(input_extra.value == ''){
        bError = true;
      }
  
  
      return bError;
    }
  
    registrar_libreria(nombre_libreria, nombre_juridico, codigo_sucursal, email, telefono,
      provincia, canton, extra);
 
            
                
        } 
        
        





boton_registrar.addEventListener("click", obtener_datos );